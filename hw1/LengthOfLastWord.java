
//        Given a string s consisting of some words separated by some number of spaces, return the length of the last word in the string.
//        A word is a maximal substring consisting of non-space characters only.

import java.util.Scanner;

public class LengthOfLastWord {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(lengthOfLastWord(scanner.nextLine()));
        scanner.close();
    }

    public static int lengthOfLastWord(String s) {
        String[] stingArray = s.split(" ");

        return stingArray[stingArray.length - 1].length();
    }
}
