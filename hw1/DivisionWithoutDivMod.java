public class DivisionWithoutDivMod {

    public static void main(String[] args) {
        div(9, 4);
    }

    public static void div (int x, int y){
        int div = 0;

        while (x >= y) {
            x -= y;
            div++;
        }
        System.out.print(div);
    }
}
