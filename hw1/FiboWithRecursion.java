public class FiboWithRecursion {

    public static void main(String[] args) {
        System.out.println(numOfFibo(10));
    }

    public static int numOfFibo(int num){
        if (num <= 1) {
            return 0;
        } else if (num == 2) {
            return 1;
        } else {
            return numOfFibo(num - 1) + numOfFibo(num - 2);
        }
    }
}
