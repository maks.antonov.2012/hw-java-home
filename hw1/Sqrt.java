
//        Given a non-negative integer x, compute and return the square root of x.
//        Since the return type is an integer, the decimal digits are truncated, and only the integer part of the result is returned.
//        Note: You are not allowed to use any built-in exponent function or operator, such as pow(x, 0.5) or x ** 0.5.

import java.util.InputMismatchException;
import java.util.Scanner;

public class Sqrt {

    public static void main(String[] args) {
        System.out.println("Введите число");
        try (Scanner scanner = new Scanner(System.in);){
            System.out.println(mySqrt(scanner.nextInt()));
        }catch (InputMismatchException e){
            System.out.println("Вы ввели не числовое значение. Перезапустите программу");
        }

    }

    public static int mySqrt(int num) {
        long r = num;

        while (r*r > num){
            r = (r + num/r) / 2;
        }

        return (int) r;
    }
}
