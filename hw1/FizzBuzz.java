
//Fizz-buzz: если число делится на 3 –fizz, на 5-buzz, на 3 и 5 –fizzbuzz

import java.util.InputMismatchException;
import java.util.Scanner;

public class FizzBuzz {
    public static void main(String[] args) {
        final String FIZZ = "fizz";
        final String BUZZ = "buzz";

        try (Scanner scanner = new Scanner(System.in);){
            int num = scanner.nextInt();

            if (num %3 == 0 && num %5 == 0){
                System.out.println(FIZZ + BUZZ);
            }else if (num %3 == 0){
                System.out.println(FIZZ);
            }else if (num %5 == 0){
                System.out.println(BUZZ);
            }
        }catch (InputMismatchException e){
            System.out.println("Вы ввели не числовое значение. Перезапустите программу");
        }
    }
}
