
//        Given an integer x, return true if x is palindrome integer.
//        An integer is a palindrome when it reads the same backward as forward.
//        For example, 121 is a palindrome while 123 is not.

import java.util.InputMismatchException;
import java.util.Scanner;

public class Palindrome {

    public static void main(String[] args) {

        try (Scanner scanner = new Scanner(System.in)){
            if(palindrome(scanner.nextInt())){
                System.out.println("ПАЛИНДРОМ");
            }else {
                System.out.println("НЕ ПАЛИНДРОМ");
            }
        }catch (InputMismatchException e){
            System.out.println("Вы ввели не числовое значение. Перезапустите программу");
        }
    }

    public static boolean palindrome(int num){
        if(num < 0){
            return false;
        }

        int palindrome = num;
        int reverse = 0;

        while (palindrome != 0){
            int reminder = palindrome % 10;
            reverse = reverse * 10 + reminder;
            palindrome = palindrome / 10;
        }

        return num == reverse;
    }
}
